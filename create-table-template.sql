
DROP TABLE IF EXISTS article;

CREATE TABLE article (
    id INT AUTO_INCREMENT PRIMARY KEY,
    title VARCHAR (265),
    text TEXT,
    image VARCHAR(128),
    date DATE 
);
/* 2022-05-02 16:36:54 [6 ms] */ 
INSERT INTO article (title, text, image , date) VALUES
("Cascata do Caracol",
"Cachoeira do Caracol é uma cachoeira de 130 metros,
 localizada a 7 quilômetros do município de Canela, 
 no Parque Estadual do Caracol.É formada pelo rio Caracol e 
 corta de falésias basálticas na Serra Geral, caindo no Vale da Lageana.
 As quedas estão situadas entre a zona pinheiral (floresta de pinheiros) 
 do Planalto Brasileiro e a Mata Atlântica do litoral sul.
 A cachoeira se formou nas rochas basálticas da formação Serra Geral e 
 possui duas cascatas. A cascata superior está localizada a aproximadamente 100 m antes,
 da segunda cascata, que cai sobre uma borda saliente do penhasco.
 As Cataratas do Caracol há tempos atraem visitantes e são a segunda atração turística natural mais popular do Brasil,
 atrás apenas das Cataratas do Iguaçu. Em 2009, recebeu mais de 289.000 visitantes.
 Há uma torre de observação de 30 metros que oferece elevador e vista panorâmica, 
 além de um teleférico que dá aos turistas uma visão aérea da cachoeira.",
 "https://pixabay.com/pt/photos/caracol-cachoeira-brasil-62901/",
 "2021-10-10"),

 ("Cristo Redentor",
 "Cristo Redentor é uma estátua art déco que retrata Jesus Cristo, localizada no topo do morro do Corcovado,
  a 709 metros acima do nível do mar, com vista para parte considerável da cidade brasileira do Rio de Janeiro.
  Feito de concreto armado e pedra-sabão, tem trinta metros de altura (uma das maiores estátuas do mundo), 
  sem contar os oito metros do pedestal. Seus braços se esticam por 28 metros de largura e a estrutura pesa 1145 toneladas.
  O monumento é parte de um santuário católico e a Arquidiocese do Rio de Janeiro administra a estátua e o platô do mirante, 
  além de também ser responsável pela manutenção e pelas celebrações na área. O direito de gerenciar o local foi concedio pela,
  União à Arquidiocese do Rio na década de 1930, mas o acesso à estátua é realizado através do Parque Nacional da Tijuca,
  que é administrado pelo Instituto Chico Mendes de Conservação da Biodiversidade (ICMBio).
  A ideia de construir uma grande estátua no alto do Corcovado foi sugerida pela primeira vez em meados do século XIX,
  mas foi o Círculo Católico do Rio de Janeiro que conseguiu as doações necessárias para colocar a ideia em prática no início do século XX.
  O monumento foi construído na França a partir de 1922 através de uma colaboração entre os brasileiros Heitor da Silva Costa e Carlos Oswald,
  os franceses Paul Landowski e Albert Caquot e o romeno Gheorghe Leonida. Sua inauguração ocorreu no dia 12 de outubro de 1931, dia de Nossa Senhora Aparecida.",
  "https://pixabay.com/pt/photos/brasil-rio-panorama-turismo-oceano-4809011/",
  "2021-11-11"),

("Cataratas do Iguaçu",
 "Cataratas do Iguaçu (em castelhano: Cataratas del Iguazú) é um conjunto de cerca de 275 quedas de água no rio Iguaçu (na Bacia hidrográfica do rio Paraná),
  localizada entre o Parque Nacional do Iguaçu, Paraná, no Brasil, e o Parque Nacional Iguazú em Misiones, na Argentina, na fronteira entre os dois países.
  A área total de ambos os parques nacionais corresponde a 250 mil hectares de floresta subtropical e é considerada Patrimônio Natural da Humanidade.
  O parque nacional argentino foi criado em 1934, enquanto o parque brasileiro foi inaugurado em 1939. Ambas as áreas de proteção com o propósito de administrar,
  e preservar o manancial de água que representa essa catarata e o conjunto do meio ambiente ao seu redor. Os parques tanto brasileiro como argentino passaram,
  a ser considerados Patrimônio da Humanidade em 1984 e 1986, respectivamente. Desde 2002, o Parque Nacional do Iguaçu é um dos sítios geológicos brasileiros.
  Historicamente, o primeiro europeu a achar as Cataratas do Iguaçu foi o espanhol Álvar Núñez Cabeza de Vaca, no ano de 1541.[2] Atualmente, é o segundo local mais visitado por estrangeiros no Brasil.
  Em época de chuva, as Cataratas do Iguaçu chegam a ser a 3ª maior do mundo em volume de água. Sua vazão chega a aumentar 10 vezes,[4] chegando a 11,3 mil metros cúbicos por segundo, quando o normal é 1,5 mil. ",
  "https://pixabay.com/pt/photos/iguazu-cachoeira-cai-cachoeira-322695/",
  "2021-12-12");
DROP TABLE IF EXISTS comments;
CREATE TABLE comments (
    id  INT AUTO_INCREMENT PRIMARY KEY,
    text TEXT,
    date DATE
);
INSERT INTO 
comments 
(text,date)
VALUES 
(
 "Paisagem belíssima e momentos de conexão com a natureza.
  No Parque do Caracol você tem uma belíssima vista para a Cascata do Caracol,
  e ainda tem a opção de fazer algumas trilhas que lhe levarão a paidagens belíssimas,
  e de alta conexão com a Natureza. Além destes passeios você também tem várias lojinhas 
  de souvenirs e algumas de queijos e vinhos (vinhos coloniais deliciosos!!).
  É um passeio perfeito para se fazer em família ou com amigos. A taxa que pagamos foi de R$ 30,00,
  a qual dá acesso ao parque mas não à torre suspensa na qual se tem maior abrangência do parque. 
  Não subimos na torre mas tivemos belíssimas paisagens!! Compartilharei algumas com vocês!! Estando em Canela ",
  "2022-04-02"),

  (
  "Fomos de Uber até o Paineiras e de lá fomos de van até o Cristo,
   é tudo bem organizado e rápido. Confesso que tinha uma imagem diferente do Cristo na minha cabeça,
   achava que a estátua em si era bem maior, a vista realmente é muito bonita, antes mesmo de chegar no Cristo, 
   impressiona a primeira vista, mas o espaço no pé no Cristo é pequeno e fica muita gente aglomerada. Impossível tirar,
   uma foto sem sair outras pessoas na foto rs. Depois que fui a outros lugares no Rio, como o Pão de Açúcar, 
   o Cristo acabou ficando em terceiro ou quarto lugar, pra mim, no que diz em respeito a beleza da paisagem. 
   Acho que esperava mais desse lugar, mas recomendo que todo mundo vá pelo menos uma vez para conhecer.
   Tem uma lanchonete como estrutura, serviço ok. Bom tomar um café e apreciar a linda vista.",
   "2020-02-04"),

   (
   "O passeio da rota das cataratas é maravilhoso. Voltei depois de uns 10 anos.
    O que chamou a atenção foi o desleixo com os ônibus que fazem o transporte dentro do parque.
    O que me levou estava visivelmente deteriorado, o som com informações sobre o passeio e as paradas,
    não funcionava apesar de mais de dez tentativas. Um local famoso no mundo todo, que recebe milhares de turistas,
    tem que estar impecável para demonstrar profissionalismo no atendimento e informações aos turistas.",
   "2019-04-04");

  --  SELECT * FROM article;

   