package co.simplon.promo18.springblog.repository;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import co.simplon.promo18.springblog.entity.ArticleEntity;

@Repository
public class ArticleRepository {
    
    @Autowired
    private DataSource dataSource;

    public List<ArticleEntity> findAll() {
        List<ArticleEntity> list = new ArrayList<>();
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM article");

            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                ArticleEntity article = new ArticleEntity(
                        rs.getInt("id"),
                        rs.getString("title"),
                        rs.getString("text"),
                        rs.getString("image"),
                        rs.getDate("date").toLocalDate());
            

                list.add(article);
            }
        } catch (SQLException e) {
            
            e.printStackTrace();
        }

        return list;
    }

public ArticleEntity findById(int id) {
    try (Connection connection = dataSource.getConnection()) {
        PreparedStatement stmt = connection.prepareStatement("SELECT * FROM article WHERE id=?");

        stmt.setInt(1, id);

        ResultSet rs = stmt.executeQuery();

        if (rs.next()) {
            ArticleEntity article = new ArticleEntity(
                rs.getInt("id"),
                rs.getString("title"),
                rs.getString("text"),
                rs.getString("image"),
                rs.getDate("date").toLocalDate());

            return article;
        }
    } catch (SQLException e) {
        
        e.printStackTrace();
        throw new RuntimeException("Database access error");
    }

    return null;
}

public void save(ArticleEntity article) {
    try (Connection connection = dataSource.getConnection()) {
        PreparedStatement stmt = connection.prepareStatement
        ("INSERT INTO article (title, text, image, date) VALUES (?,?,?,?);", PreparedStatement.RETURN_GENERATED_KEYS);

        stmt.setString(1, article.getTitle());
        stmt.setString(2,article.getText());
        stmt.setString(3, article.getImage());
        stmt.setDate(4, Date.valueOf(article.getDate()));


        stmt.executeUpdate();

        ResultSet rs = stmt.getGeneratedKeys();
        if(rs.next()) {
            article.setId(rs.getInt(1));
        }
     
    } catch (SQLException e) {
        
        e.printStackTrace();
        throw new RuntimeException("Database access error");
    }

}
public boolean update(ArticleEntity article) {
    try (Connection connection = dataSource.getConnection()) {
        PreparedStatement stmt = connection.prepareStatement("UPDATE article SET title=?,image=?,date=? WHERE id=?");
        
        stmt.setString(1, article.getTitle());
        stmt.setString(2,article.getText());
        stmt.setString(3, article.getImage());
        stmt.setDate(2, Date.valueOf(article.getDate()));

        return stmt.executeUpdate() == 1;
        

    } catch (SQLException e) {
        e.printStackTrace();

    }
    return false;
}

public boolean deleteById(int id) {
    try (Connection connection = dataSource.getConnection()) {
        PreparedStatement stmt = connection.prepareStatement("DELETE FROM article WHERE id=?");

        stmt.setInt(1, id);

        return stmt.executeUpdate() == 1;

    } catch (SQLException e) {
        e.printStackTrace();

    }
    return false;
    }
}



