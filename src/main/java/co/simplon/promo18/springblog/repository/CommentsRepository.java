package co.simplon.promo18.springblog.repository;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

//import co.simplon.promo18.springblog.entity.ArticleEntity;
import co.simplon.promo18.springblog.entity.CommentsEntity;
@Repository
public class CommentsRepository {

    @Autowired
    private DataSource dataSource;

    public List<CommentsEntity> findAll() {
        List<CommentsEntity> list = new ArrayList<>();
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM comments");

            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                CommentsEntity comments = new CommentsEntity(
                        rs.getInt("id"),
                        rs.getString("text"),
                        rs.getDate("date").toLocalDate());

                list.add(comments);
            }
        } catch (SQLException e) {

            e.printStackTrace();
        }

        return list;
    }

    public CommentsEntity findById(int id) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM comments WHERE id=?");

            stmt.setInt(1, id);

            ResultSet rs = stmt.executeQuery();

            if (rs.next()) {
                CommentsEntity comments = new CommentsEntity(
                        rs.getInt("id"),
                        rs.getString("text"),
                        rs.getDate("date").toLocalDate());

                return comments;
            }
        } catch (SQLException e) {

            e.printStackTrace();
            throw new RuntimeException("Database access error");
        }

        return null;
    }

    public void save(CommentsEntity comments) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("INSERT INTO comments (text, date) VALUES (?,?)",
                    Statement.RETURN_GENERATED_KEYS);

            stmt.setString(1, comments.getText());
            stmt.setDate(2, Date.valueOf(comments.getDate()));

            stmt.executeUpdate();

            ResultSet rs = stmt.getGeneratedKeys();
            if (rs.next()) {
                comments.setId(rs.getInt(1));
            }

        } catch (SQLException e) {

            e.printStackTrace();
            throw new RuntimeException("Database access error");
        }

    }

    public boolean deleteById(int id) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("DELETE FROM comments WHERE id=?");

            stmt.setInt(1, id);

            return stmt.executeUpdate() == 1;

        } catch (SQLException e) {
            e.printStackTrace();

        }
        return false;
    }
}
