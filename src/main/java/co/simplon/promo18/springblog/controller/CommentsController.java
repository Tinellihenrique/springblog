package co.simplon.promo18.springblog.controller;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import co.simplon.promo18.springblog.entity.CommentsEntity;
import co.simplon.promo18.springblog.repository.CommentsRepository;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class CommentsController {

    @Autowired
    CommentsRepository comRepo;

    @GetMapping("/api/comments")
    public List<CommentsEntity> getAllComment(){
        List<CommentsEntity> list=comRepo.findAll();
        return list;

    }
    @GetMapping("/api/comments/{id}") 
    public CommentsEntity one(@PathVariable int id) {
        CommentsEntity comments = comRepo.findById(id);

        return comments;
    }
    
    @PostMapping("/api/comments")
    public CommentsEntity add(@RequestBody CommentsEntity comments) {
        if(comments.getDate() == null){
            comments.setDate(LocalDate.now());
        }

        comRepo.save(comments);
        
        return comments;
    }
    @DeleteMapping("/api/comments/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT) 
    public void delete(@PathVariable int id) {
        if(!comRepo.deleteById(id)){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

}


