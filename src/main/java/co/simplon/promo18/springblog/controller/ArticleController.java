package co.simplon.promo18.springblog.controller;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import co.simplon.promo18.springblog.entity.ArticleEntity;
import co.simplon.promo18.springblog.repository.ArticleRepository;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class ArticleController {
   
    @Autowired
    ArticleRepository comRepo;

    @GetMapping("/api/article")
    public List<ArticleEntity> all() {
        return comRepo.findAll();
    }

    
    @GetMapping("/api/article/{id}") 
    public ArticleEntity one(@PathVariable int id) {
        ArticleEntity comments = comRepo.findById(id);

        return comments;
    }
    
    @PostMapping("/api/article")
    public ArticleEntity add(@RequestBody ArticleEntity article) {
        if(article.getDate() == null){
            article.setDate(LocalDate.now());
        }

        comRepo.save(article);
        
        return article;
    }

    @DeleteMapping("/api/article/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT) 
    public void delete(@PathVariable int id) {
        if(!comRepo.deleteById(id)){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

}



