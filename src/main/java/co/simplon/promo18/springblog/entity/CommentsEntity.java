package co.simplon.promo18.springblog.entity;

import java.time.LocalDate;

public class CommentsEntity {
        private Integer id;
        private String text;
        private LocalDate date;
        public CommentsEntity() {
        }
        public CommentsEntity(String text, LocalDate date) {
                this.text = text;
                this.date = date;
        }
        public CommentsEntity(Integer id, String text, LocalDate date) {
                this.id = id;
                this.text = text;
                this.date = date;
        }
        public Integer getId() {
                return id;
        }
        public void setId(Integer id) {
                this.id = id;
        }
        public String getText() {
                return text;
        }
        public void setText(String text) {
                this.text = text;
        }
        public LocalDate getDate() {
                return date;
        }
        public void setDate(LocalDate date) {
                this.date = date;
        }
    
}

