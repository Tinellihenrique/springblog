package co.simplon.promo18.springblog.entity;

import java.time.LocalDate;

public class ArticleEntity {
    private Integer id;
    private String title;
    private String text;
    private String image;
    private LocalDate date;

    public ArticleEntity() {
    }

    public ArticleEntity(String title, String text, String image, LocalDate date) {
        this.title = title;
        this.text = text;
        this.image = image;
        this.date = date;
    }

    public ArticleEntity(Integer id, String title, String text, String image, LocalDate date) {
        this.id = id;
        this.title = title;
        this.text = text;
        this.image = image;
        this.date = date;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

}
